#+title:      convertmyvideo
#+date:       20221120120254
#+filetags:   :linux:thm:writeup:
#+identifier: 20221120T120254

[[file:img/convertmyvideo-00.jpg]]

* [X] Initial Recon
** [X] =nmap=
Let's run our initial nmap scan.

#+begin_src bash
sudo nmap -sCV -Pn -oA nmap/allports [convertmyvideo IP]
#+end_src

I normally run this with the flag =-p-= but for some reason, the remote machine is very slow so I just tested the first 1000 ports.

*** Results

#+begin_src bash
Nmap scan report for 10.10.121.62
Host is up (0.39s latency).
Not shown: 998 closed tcp ports (reset)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 651bfc741039dfddd02df0531ceb6dec (RSA)
|   256 c42804a5c3b96a955a4d7a6e46e214db (ECDSA)
|_  256 ba07bbcd424af293d105d0b34cb1d9b1 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Site doesn't have a title (text/html; charset=UTF-8).
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Nov 19 23:04:23 2022 -- 1 IP address (1 host up) scanned in 25.38 seconds
#+end_src

* [X] Initial Foothold
Let's inspect the service running on port 80.

It shows us a page that says "Convert My Video". I am guessing this is supposed to take a Youtube URL and convert it to MP3 or something.

[[file:img/convertmyvideo-01.jpg]]

I have placed a legitimate Youtube URL to the input field and inspected the request in =ZAProxy=.

[[file:img/convertmyvideo-02.jpg]]

Unfortunately, the request times out.

It looks like our input is not being sanitized in any way. We can try to fuzz the input field and trigger and error. This way we can have an idea what happens in the backend.

[[file:img/convertmyvideo-03.jpg]]

Looks like the user input is passed directly to a command/ to =sh= in the server.

This means this might be vulnerable to command injection.

Passing =ls= command as user input gives us a more verbose error message.

[[file:img/convertmyvideo-04.jpg]]

Playing around on possible iterations of commands, we can see that with the payload =%26ls= gives us a directory listing.

[[file:img/convertmyvideo-05.jpg]]

I had trouble working around the issue with spaces in the command until I saw a thread that uses =$IFS= instead of spaces.

Command with spaces:

[[file:img/convertmyvideo-06.jpg]]

Command with =$IFS=:

[[file:img/convertmyvideo-07.jpg]]

I'll copy the output into a text file and clean it up a bit so it's easier to read.

#+begin_src bash
{"status":127,"errors":"WARNING: Assuming --restrict-filenames since file system encoding cannot encode all characters. Set the LC_ALL environment variable to fix this.\n
Usage: youtube-dl [OPTIONS] URL [URL...]\n
\n
youtube-dl: error: You must provide at least one URL.\n
Type youtube-dl --help to see a list of all options.\n
sh: 1: -f: not found\n","url_orginal":";ls$IFS-lah;","output":"total 36K\n
drwxr-xr-x 6 www-data www-data 4.0K Apr 12  2020 .\n
drwxr-xr-x 3 root     root     4.0K Apr 12  2020 ..\n
-rw-r--r-- 1 www-data www-data  152 Apr 12  2020 .htaccess\n
drwxr-xr-x 2 www-data www-data 4.0K Apr 12  2020 admin\n
drwxrwxr-x 2 www-data www-data 4.0K Apr 12  2020 images\n
-rw-r--r-- 1 www-data www-data 1.8K Apr 12  2020 index.php\n
drwxrwxr-x 2 www-data www-data 4.0K Apr 12  2020 js\n
-rw-rw-r-- 1 www-data www-data  205 Apr 12  2020 style.css\n
drwxr-xr-x 2 www-data www-data 4.0K Apr 12  2020 tmp\n
","result_url":"\/tmp\/downloads\/6379c195bafb3.mp3"}
#+end_src

We can see that there is an admin directory. That might be interesting to us. Let's try to list out that directory's contents.

[[file:img/convertmyvideo-08.jpg]]

#+begin_src bash
{"status":127,"errors":"sh: 1: -f: not found\n
WARNING: Assuming --restrict-filenames since file system encoding cannot encode all characters. Set the LC_ALL environment variable to fix this.\n
Usage: youtube-dl [OPTIONS] URL [URL...]\n\nyoutube-dl: error: You must provide at least one URL.\nType youtube-dl --help to see a list of all options.\n
","url_orginal":"&ls$IFS-la$IFS.\/admin&","output":"total 24\n
drwxr-xr-x 2 www-data www-data 4096 Apr 12  2020 .\n
drwxr-xr-x 6 www-data www-data 4096 Apr 12  2020 ..\n
-rw-r--r-- 1 www-data www-data   98 Apr 12  2020 .htaccess\n
-rw-r--r-- 1 www-data www-data   49 Apr 12  2020 .htpasswd\n
-rw-r--r-- 1 www-data www-data   39 Apr 12  2020 flag.txt\n
-rw-rw-r-- 1 www-data www-data  202 Apr 12  2020 index.php\n
","result_url":"\/tmp\/downloads\/637b2ea950a1f.mp3"}
#+end_src

We might be able to see what's inside =flag.txt= if we go this route.

* [X] User flag
=cat= out what's inside =flag.txt=.

[[file:img/convertmyvideo-09.jpg]]

#+html: <details><summary>spoiler warning</summary><tt>
flag{0d8486a0c0c42503bb60ac77f4046ed7}
#+html: </tt></details>

* [X] Privilege Escalation
There are other files in the =/admin= directory. One interesting file is the =.htpasswd=. According to [[https://httpd.apache.org/docs/2.4/programs/htpasswd.html][Apache HTTP Server Project websit]]e: =htpasswd= is used to create and update the flat-files used to store usernames and password for basic authentication of HTTP users.

Let's see the contents of the file:

[[file:img/convertmyvideo-10.jpg]]

Looks like we have a user and a hash:

#+begin_src bash
itsmeadmin:$apr1$tbcm2uwv$UP1ylvgp4.zLKxWj8mc6y\/
#+end_src

Note that the backslash might be just an escape character, so we can also consider the credentials to be:

#+begin_src bash
itsmeadmin:$apr1$tbcm2uwv$UP1ylvgp4.zLKxWj8mc6y/
#+end_src

Let's run =hashcat= against the hash to see if we can crack it.

[[file:img/convertmyvideo-11.jpg]]

=hashcat= identifies the module that we need to use is 1600.

[[file:img/convertmyvideo-12.jpg]]

The final credentials are =itsmeadmin:jessie=.

Trying out the credentials using =ssh= does not seem to work.

[[file:img/convertmyvideo-13.jpg]]

We may need to look for another way in.

I should have tried dirbusting at the beginning of the recon phase, but here it is.

[[file:img/convertmyvideo-14.jpg]]

Looks like there is an =/admin= endpoint that might be of interest to us.

There is a login prompt when we visit the page. Let's try the credentials that we obtained: =itsmeadmin:jessie=.

We get in with a page with a single button. Looks like it sends a command to the server as we can see at the bottom left of the page when we hover the mouse cursor over the button.

[[file:img/convertmyvideo-15.jpg]]

Here's what the button does. Looks like this is also an avenue to execute commands on the server?

[[file:img/convertmyvideo-16.jpg]]

[[file:img/convertmyvideo-17.jpg]]

Maybe we will have luck here in getting a reverse shell.

Run a server using =http.server= at port 80 and send a =wget= request.

[[file:img/convertmyvideo-18.jpg]]

[[file:img/convertmyvideo-19.jpg]]

[[file:img/convertmyvideo-20.jpg]]

Let's enumerate further for an escalation vector.

[[file:img/convertmyvideo-21.jpg]]

[[file:img/convertmyvideo-22.jpg]]

There is an exploit mentioned by =linpeas= regarding pwnkit however, it is not effective when I tried it out.

Let's use =pspy=.

[[file:img/convertmyvideo-23.jpg]]

[[file:img/convertmyvideo-24.jpg]]

We can see that there is a script that is being run with root privileges. Let's check it out.

[[file:img/convertmyvideo-25.jpg]]

It removes the =downloads= directory in =/var/www/html/=  directory. Looks like we have write permissions on the file. Maybe we can add a reverse shell script to the file and we can catch it using =pwncat=.

[[file:img/convertmyvideo-26.jpg]]

[[file:img/convertmyvideo-27.jpg]]

* [X] Root flag
Let's get the root flag.

[[file:img/convertmyvideo-28.jpg]]

#+html: <details><summary>spoiler warning</summary><tt>
flag{d9b368018e912b541a4eb68399c5e94a}
#+html: </tt></details>
