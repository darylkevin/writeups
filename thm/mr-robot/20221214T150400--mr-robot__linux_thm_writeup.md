---
title:      "mr robot"
date:       20221214150400
tags:       ["linux", "thm", "writeup"]
identifier: "20221214T150400"
---

# Machine Information

- Machine Name: Mr. Robot
- IP Address: [Variable IP address depending if you can solve the box in one sitting]

## Scanning and Enumeration
### `nmap`

Describe the steps you took to scan and enumerate the machine, including the tools and commands you used.

## Initial Exploitation
### Fuzzing/ Directory busting

Describe the initial vulnerability you discovered and how you exploited it to gain access to the machine.

## Privilege Escalation

Describe any additional vulnerabilities you discovered and how you exploited them to escalate your privileges on the machine.

## User Flag

Include a screenshot or copy of the user flag that you obtained.

## Root Flag

Include a screenshot or copy of the root flag that you obtained.

## Conclusion

Summarize your experience with the machine and what you learned from it.
