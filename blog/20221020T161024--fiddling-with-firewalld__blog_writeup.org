#+title:      Fiddling with firewalld
#+date:       20221020161024
#+filetags:   :blog:writeup:
#+identifier: 20221020T161024

Before delving into information technology and security, I didn't put too much thought into how I use computers. I did not know how to use and configure firewalls yet alone, make my computer secure.

After taking a completing the CompTIA trifecta, I had a newly found appreciation with network security. I want to share what I learned to you with emphasis on how to apply it to a Linux machine.

* What is a firewall?

According to the definition from [[https://en.wikipedia.org][Wikipedia]], a firewall is a network security system that monitors and controls incoming and outgoing traffic based on a predetermined security rules.

Simply put in networking terms, a firewall is simply a packet filter. It allows certain packets to traverse your network from authorized or trusted sources and blocks packets from traversing if it comes from unauthorized or unknown sources.

* Presenting firewalld

From the [[https://firewalld.org][project homepage]]

Firewalld provides a dynamically managed firewall with support for network/firewall zones that define the trust level of network connections or interfaces.

** Installation

Firewalld should be available for most, if not all, Linux distributions.

For Arch:

#+begin_src sh
sudo pacman -S firewalld
#+end_src

For Fedora, CentOS, RHEL:

#+begin_src sh
sudo dnf install firewalld
#+end_src

For openSUSE, SLE:

#+begin_src sh
sudo zypper in firewalld
#+end_src

To enable firewalld, use the command:

#+begin_src sh
sudo systemctl enable --now firewalld
#+end_src

To check firewalld status, use the command:

#+begin_src sh
sudo systemctl status firewalld
#+end_src

Firewall rules can be controlled with the =firewall-cmd= command in your terminal. If you want a GUI interface, =firewall-config= is available for your use and should come preinstalled with the firewalld package.

** Firewall zones

Firewalld manages a set of predefined firewall rules with the use of zones. These zones are based on levels of trust a user has on an interface and its connection.

Different zones will have different rules, i.e. a trusted zone may have more ports open and a less trusted zone will be more restrictive.

*** Levels of trust in zones

Listed below are the default zones available and they are ranked from least trusted to most trusted:

- **Drop Zone** - all incoming connections are dropped without any warnings.
- **Block Zone** - similar with drop zone but with ICMP replies enabled.
- **Public Zone** - interface is connected to an untrusted network but may allow selected connections on case basis.
- **External Zone** - this zone is commonly used if your firewall is used as a gateway. This is configured for NAT.
- **Internal Zone** - computers are trusted and some extra services are available.
- **DMZ Zone** - only certain _incoming_ connections are allowed.
- **Work Zone** - majority of hosts are trusted in the network. More services are allowed here.
- **Home Zone** - Your home network. Trust all hosts in the network. More services are allowed.
- **Trusted Zone** - Everyone is trusted. This should be used _very rarely and carefully_.

A zone's rules are automatically applied to an interface which is in a zone. An interface can only be in a single zone.

** Firewalld commands

To check which zone your interface is currently on:

#+begin_src sh
firewall-cmd --get-default-zone
#+end_src

To check which interfaces are active on which zone:

#+begin_src sh
firewall-cmd --get-active-zone
#+end_src

To check what rules are applied on the defaul

#+begin_src sh
firewall-cmd --list-all
#+end_src

To check what zones are available:

#+begin_src sh
firewall-cmd --get-zones
#+end_src

To check what rules and services are applied to a specific zone (e.g. home):

#+begin_src sh
firewall-cmd --zone=home --list-all
#+end_src

To check what rules and services are applied to all zones:

#+begin_src sh
firewall-cmd --list-all-zones
#+end_src

To change an interface's zone (e.g. from =public=  to =home=):

#+begin_src sh
firewall-cmd --zone=home --change-interface=<interface name>
#+end_src

Note that this change will not be permanent.


To change your default zone (e.g. =public= to =home=):

#+begin_src sh
firewall-cmd --set-default-zone=<name of zone>
#+end_src

To get all the services available:

#+begin_src sh
firewall-cmd --get-services
#+end_src

To get information on all the services listed by the command above, navigate to:

#+begin_src sh
cd /usr/lib/firewalld/services
#+end_src

You can =cat= the file for the description of the services listed. All these services are predefined.

If you want to add a particular service to your zone, you can run the command:

#+begin_src sh
firewall-cmd --add-service=<name of service>
#+end_src

Please note that the service added by the above method will be gone after a reboot/ restart of the service.

If you want a service to be permanently added to a zone, run the command:

#+begin_src sh
firewall-cmd --permanent --add-service=<name of service>
#+end_src

Always add the =--permanent= flag to add changes permanently.

After making changes, reload your firewalld:

#+begin_src sh
firewall-cmd --reload
#+end_src

If you want to list all services activated for a particular zone:

#+begin_src sh
firewall-cmd --zone=<name of zone> --list-services
#+end_src

In case a service is not available on the default list (e.g. a custom service) and you want to add that service's particular port, you can use:

#+begin_src sh
firewall-cmd --permanent --add-port=<port number>/<protocol>
#+end_src

For example:

#+begin_src sh
firewall-cmd --permanent --add-port=9090/tcp
#+end_src

Port ranges can also be defined by the above command:

#+begin_src sh
firewall-cmd --permanent --add-port=9000-9090/tcp
#+end_src

To define a service (i.e. create a custom service) which is not included in one of the defaults, you can copy one of the samples from =/usr/lib/firewalld/services/= into =/etc/firewalld/services/= directory and edit the copied service file to define all that's needed i.e. name, description, ports, and protocols. Note that you can define multiple ports and protocols here.

#+begin_src sh
cp /usr/lib/firewalld/services/ssh.xml /etc/firewalld/services/custom_service.xml
vim /etc/firewalld/services/custom_service.xml
#+end_src

To add a trusted source (host/s, network) to your firewall rules, use:

#+begin_src sh
firewall-cmd --permanent --zone=<desired zone> --add-source=<ip address/subnet mask>
#+end_src
#+begin_src sh
firewall-cmd --permanent --zone=public --add-source=192.168.0.222/24
#+end_src

All traffic from trusted source is allowed thru the above command.

Don't forget to reload your firewalld.

To get which zone an interface is placed:

#+begin_src sh
firewall-cmd --get-zone-of-interface=<name of interface>
#+end_src

Use =ifconfig= or =ip a= to get your interface name.

To get which services will be available after a reboot:

#+begin_src sh
firewall-cmd --get-services --permanent
#+end_src

** Panic mode
If you want to shutoff all connections immediately, like when you want to protect yourself against an attack, you can use =panic mode=.

#+begin_src sh
firewall-cmd --panic-on
#+end_src

If you want to turn =panic mode= off:

#+begin_src sh
firewall-cmd --panic-off
#+end_src

To check if you have =panic mode= enabled or not:

#+begin_src sh
firewall-cmd --query-panic
#+end_src

It goes without saying that it is __not__ a good idea to activate =panic mode= on a remote machine as it will drop off __all__ connections, including your remote connection to that machine. Be very careful with this one.

** Rule Ordering

Firewall rules are applied in a particular order in order to avoid conflict. All zones follow this order from top to bottom:

- Port forwarding or masquerading rule
- Logging rules
- Allow rules
- Deny rules

If some rules interact/contradict with each other, the first rule that matches gets implemented.

** Rich Rules

Firewalld allows more fine-grained control by the use of rich rules. Rich rules are custom firewall rules.

More details can be found in the =man= pages: =man 5 firewalld.richlanguage=.

#+begin_src sh
General rule structure

           rule
             [source]
             [destination]
             service|port|protocol|icmp-block|icmp-type|masquerade|forward-port|source-port
             [log]
             [audit]
             [accept|reject|drop|mark]
#+end_src

An example of a rich rule being declared is shown below:

#+begin_src sh
firewall-cmd --permanent --zone=public --add-rich-rules='rule family="ipv4" source address="192.168.0.0/24" service name="tftp" log prefix="tftp" level="info" limit value="1/m" accept'
#+end_src

In the above example, you can see just how much fine tuning we can apply to our firewall when we use rich rules.

* References

[[https://www.youtube.com/watch?v=jgErVHBz7XI][RHCE Training - Configuring Firewalld in RHEL 7]]
