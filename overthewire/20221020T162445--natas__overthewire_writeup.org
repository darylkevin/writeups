#+title:      Natas
#+date:       20221020162445
#+filetags:   :overthewire:writeup:
#+identifier: 20221020T162445

* Natas
After finishing all the current =bandit= levels, I opted to try out the next level which is =natas=.

* What is Natas?

#+begin_quote
Natas teaches the basics of serverside web-security.

Each level of natas consists of its own website located at =http://natasX.natas.labs.overthewire.org=, where =X= is the level number. There is =no SSH login=. To access a level, enter the username for that level (e.g. natas0 for level 0) and its password.

Each level has access to the password of the next level. Your job is to somehow obtain that next password and level up. All passwords are also stored in =/etc/natas_webpass/=. E.g. the password for =natas5= is stored in the file =/etc/natas_webpass/natas5= and only readable by natas4 and natas5.

Start here:

=Username: natas0=
=Password: natas0=
=URL:      http://natas0.natas.labs.overthewire.org=
#+end_quote

This wargame was developed in association with the [[http://www.nessos-project.eu/][NESSoS FP7 project]].

* Level 0
From the welcome screen, we can see that to enter =level0= we will need to go to the target URI and enter the credentials.

#+begin_quote
=Username: natas0=
=Password: natas0=
=URL:      http://natas0.natas.labs.overthewire.org=
#+end_quote

* Natas Level 0 → Level 1

We are then greeted with the following page.

[[file:img/natas0-0.png]]

#+begin_quote
You can find the password for the next level on this page.
#+end_quote

Aside from that, there is nothing more displayed in the page.

In this situation, it is helpful if you can check the =page source= of the webpage. Not all are rendered and seen by the visitor of the page, such as comments. To do so, right-click on the page and select =View Page Source=.

We can see that in a comment, there is the password for =natas1=.

[[file:img/natas0-1.png]]

#+begin_src
<!--The password for natas1 is gtVrDuiDfck831PqWsLEZy5gyDz1clto -->
#+end_src

* Natas Level 1 → Level 2

Login using below credentials and the password that we obtained from the previous level.

#+begin_quote
=Username: natas1=
=URL:      http://natas1.natas.labs.overthewire.org=
#+end_quote

On the page, it says that the password is also on the page but right-click has been disabled.

[[file:img/natas1-0.png]]

How can we then view the password if we can't right click? Fortunately, there are several options to do so:

1. You can use the keyboard shortcuts to view page source.

#+begin_quote
- *Firefox*: CTRL + U (Meaning press the CTRL key on your keyboard and hold it down. While holding down the CTRL key, press the “u” key.) Alternatively, you can go to the “Firefox” menu and then click on “Web Developer,” and then “Page Source.”

- *Edge/Internet Explorer*: CTRL + U. Or right click and select “View Source.”

- *Chrome*: CTRL + U. Or you can click on the weird-looking key with three horizontal lines in the upper right hand corner. Then click on “Tools” and select “View Source.”

- *Opera*: CTRL + U. You also can right click on the webpage and select “View Page Source.”
#+end_quote

2. You can use developer tools.

We'll use developer tools for Chromium to view the page source.

[[file:img/natas1-1.png]]

#+begin_src
<!--The password for natas2 is ZluruAthQk7Q2MqmDeTiUij2ZvWy2mBi -->
#+end_src

* Natas Level 2 → Level 3
Login using below credentials and the password that we obtained from the previous level.

#+begin_quote
=Username: natas2=
=URL:      http://natas2.natas.labs.overthewire.org=
#+end_quote

The page says there is nothing there.

[[file:img/natas2-0.png]]
